$(function(){
    $(document).on('click', '.control-edit', function () {
        var id = $(this).data('id'),
            $input = $('#edit_' + id);

        $input.toggleClass('hidden').prop('readonly', !$input.prop('readonly'));
        $('#label_' + id).text($input.val());
    });

    $('#confirm-dialog').on('show.bs.modal', function (e) {
        var $modal = $(this),
            $target = $(e.relatedTarget),
            targetID = $target.data('id'),
            targetTitle;

        if ($target.is('.control-delete')) {
            targetTitle = $('#label_' + targetID).text();
            $modal
                .find('.modal-title')
                .html('Подтвердите удаление');
            $modal
                .find('.modal-body')
                .html('Удалить "<strong>' + targetTitle + '</strong>"?');

            $modal
                .find('.btn-save')
                .text('Удалить')
                .off('click')
                .on('click', function () {
                    alert('удалено');
                    $modal.modal('hide');
                });
        }
    });

    $('#toggle_all_needs').on('click', function () {
        var $this = $(this),
            $checkboxes = $('.filter-by-needs input[type=checkbox]');

        if (!$this.data('checked')) {
            $checkboxes.prop('checked', true);
            $this.data('checked', true);
        } else {
            $checkboxes.prop('checked', false);
            $this.data('checked', false);
        }
    });

    var genericID = (function () {
        var id, num = 1;
        id = function(prefix){
            return (prefix || 'gallery') + (+new Date()) + (num++);
        };
        return function (prefix) {
            return id(prefix);
        }
    })();

    $('#add_need').on('click', function () {
        var $column = $('.filter-by-needs__column:last'),
            template = '<div class="row" id="{% id %}"><div class="col-xs-18"><div class="checkbox">' +
                '<input type="checkbox" id="check_{% id %}"><label for="check_{% id %}">' +
                '<input type="text" value="" id="edit_{% id %}" class="form-control form-control-sm"/>' +
                '<span class="checkbox-label" id="label_{% id %}"></span></label>' +
                '</div></div><div class="col-xs-3">' +
                '<span class="editor-control control-edit" data-id="{% id %}">' +
                '<i class="fa fa-lg fa-pencil text-yellow"></i>' +
                '</span></div><div class="col-xs-3">' +
                '<span class="editor-control control-delete" data-target="#confirm-dialog" data-toggle="modal" data-id="{% id %}">' +
                '<i class="fa fa-lg fa-trash-o text-yellow"></i>' +
                '</span></div></div>',
            id = genericID('needs_');

        $column.append(template.replace(/\{% id %}/g, id));
        $('#edit_'+id).focus();
    });
});