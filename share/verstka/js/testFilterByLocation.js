$(function () {
    var $regionSelect = $('#region'),
        $areaSelect = $('#area'),
        $citySelect = $('#city'),
        allRegions,
        allCities,
        filteredAreas,
        filteredCities,
        currentRegion,
        currentArea;

    $.ajax({
        url: 'http://api.vk.com/method/database.getRegions',
        dataType: 'jsonp',
        jsonp: 'callback',
        data: {
            country_id: 1
        },
        success: function (data) {
            var i, region;

            allRegions = data.response;
            $regionSelect.empty();

            for (i = 0; i < allRegions.length; i++) {
                region = allRegions[i];
                $regionSelect.append('<option value="' + region.region_id + '">' + region.title + '</option>');
            }

            $regionSelect.selectpicker('refresh');
        }
    });

    $regionSelect.on('change', function () {
        currentRegion = $(this).val();
        $areaSelect.prop('disabled', true).empty().selectpicker('refresh');
        $citySelect.prop('disabled', true).empty().selectpicker('refresh');
        filteredAreas = [];
        filteredCities = [];

        $.ajax({
            url: 'http://api.vk.com/method/database.getCities',
            dataType: 'jsonp',
            jsonp: 'callback',
            data: {
                country_id: 1,
                region_id: currentRegion,
                count: 1000
            },
            success: function (data) {
                var i, city;

                allCities = data.response;

                for (i = 0; i < allCities.length; i++) {
                    city = allCities[i];

                    if (city.area) {
                        if (!~$.inArray(city.area, filteredAreas)) filteredAreas.push(city.area)
                    }
                    else {
                        if (!~$.inArray(city.title, filteredAreas)) filteredAreas.push(city.title)
                    }
                }

                filteredAreas.sort();

                for (i = 0; i < filteredAreas.length; i++) {
                    city = filteredAreas[i];
                    $areaSelect.append('<option value="' + city + '">' + city + '</option>')
                }

                $areaSelect.prop('disabled', false).selectpicker('refresh');
            }
        });
    });

    $areaSelect.on('change', function () {
        var i, city;

        currentArea = $(this).val();
        $citySelect.prop('disabled', true).empty().selectpicker('refresh');
        filteredCities = [];

        for (i = 0; i < allCities.length; i++) {
            city = allCities[i];
            if (city.area) {
                if (city.area == currentArea) filteredCities.push(city);
            } else {
                if (city.title == currentArea) filteredCities.push(city);
            }
        }

        for (i = 0; i < filteredCities.length; i++) {
            city = filteredCities[i];
            $citySelect.append('<option value="' + city.cid + '">' + city.title + '</option>')
        }

        $citySelect.prop('disabled', false).selectpicker('refresh');
    });
});