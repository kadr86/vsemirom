$(function () {
    var $mapCanvas = $('#map-canvas'),
        $mapInput = $('#map_input'),
        $addressInput = $('#address_input'),
        $nameInput = $('#name_input'),
        map, marker, popup, geocoder, editorInitialized,
        initEditor = function () {
            var EditorControl = function (control, title, map) {
                    var controlUI = document.createElement('div'),
                        controlText = document.createElement('div'),
                        $controlUI = $(controlUI);

                    controlUI.title = title[0];
                    controlText.innerHTML = title[0];

                    control.appendChild(controlUI);
                    controlUI.appendChild(controlText);

                    $controlUI.addClass('map-control');

                    google.maps.event.addDomListener(controlUI, 'click', function () {
                        var $controlUI = $(controlUI),
                            latlng;

                        if ($controlUI.is('.pressed')) {

                            $controlUI.removeClass('pressed');
                            controlText.innerHTML = title[0];
                            controlUI.title = title[0];

                            latlng = marker.getPosition();
                            $mapInput.val(latlng.lat() + ',' + latlng.lng());
                            google.maps.event.clearListeners(map, 'click');
                        } else {
                            $controlUI.addClass('pressed');
                            controlText.innerHTML = title[1];
                            controlUI.title = title[1];

                            google.maps.event.addListener(map, 'click', function (event) {
                                marker.setPosition(event.latLng);
                            });
                        }
                    });

                },
                control = document.createElement('div');

            new EditorControl(control, ['Редактировать метку', 'Принять изменения'], map);
            control.index = 1;
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(control);

            return true;
        },
        createContent = function (title, address) {
            var template = '<div class="map-popup">{title}<p>' + address + '</p></div>';
            return title ? template.replace('{title}', '<h1>' + title + '</h1>') : template.replace('{title}', '');
        },
        placemarkByCoords = function (title, address, latlng) {
            if ($mapCanvas.data('editor') && !editorInitialized) {
                editorInitialized = initEditor();
            }

            google.maps.event.clearListeners(marker, 'click');

            marker.setPosition(latlng);
            map.panTo(latlng);
            map.setZoom(17);
            popup.setContent(createContent(title, address));

            google.maps.event.addListener(marker, 'click', function () {
                popup.open(map, marker);
            });
        },
        placemarkByAddress = function (title, address) {
            var latlng;
            if ($mapCanvas.data('editor') && !editorInitialized) {
                editorInitialized = initEditor();
            }
            geocoder.geocode({ 'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    latlng = results[0].geometry.location;
                    google.maps.event.clearListeners(marker, 'click');

                    $mapInput.val(latlng.lat() + ',' + latlng.lng());
                    marker.setPosition(latlng);

                    map.panTo(latlng);
                    map.setZoom(17);
                    popup.setContent(createContent(title, address));

                    google.maps.event.addListener(marker, 'click', function () {
                        popup.open(map, marker);
                    });
                } else {
                    console.log("Geocode was not successful for the following reason: " + status);
                }
            });
        },
        initialize = function () {
            var title = $mapCanvas.data('title'),
                address = $mapCanvas.data('address'),
                latlng = (!!$mapCanvas.data('latlng') ? $mapCanvas.data('latlng').split(',') : false),
                mapOptions = {
                    mapTypeControl: false,
                    streetViewControl: false,
                    zoomControlOptions: {
                        position: google.maps.ControlPosition.LEFT_TOP
                    },
                    scrollwheel: false,
                    zoom: 2
                };

            latlng = latlng ? new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1])) : new google.maps.LatLng(66.4, 94.25);
            mapOptions.center = latlng;

            geocoder = new google.maps.Geocoder();
            map = new google.maps.Map($mapCanvas.get(0), mapOptions);
            popup = new google.maps.InfoWindow({content: createContent(title, address)});
            marker = new google.maps.Marker({
                position: map.getCenter(),
                map: map,
                title: title
            });

            if (address) {
                placemarkByCoords(title, address, latlng);
            }
        };

    if ($mapCanvas.length) {
        google.maps.event.addDomListener(window, 'load', initialize);

        $addressInput.add($nameInput).on('blur change', function () {
            var address = $addressInput.val(),
                title = $nameInput.val();

            if (address) {
                placemarkByAddress(title, address);
            }
        });
    }
});