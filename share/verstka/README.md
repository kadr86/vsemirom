# Установка, сборка и запуск проекта
1. Установить node.js, npm и grunt-cli
2. В консоли перейти в папку с проектом и выполнить:

    `npm install`

    `grunt`

4. Если в консоли вы увидели сообщение

    `Running "watch" task`

    `Waiting...`

    значит все в порядке, грант запустился и была создана поддиректория `build` с собранными и минифицированными файлами

5. В браузере перейти по адресу  [http://localhost:5000/](http://localhost:5000/)


`Grunt` отслеживает изменения всех файлов в директориях `less`, `views`, `js`, а так же изменения в `Gruntfile.js`.
При этом происходит новая сборка и перезагрузка страницы в браузере(`livereload`).

Для установки пакетов в проект используется `bower`. Путь к директории `vendor` искать в `bower.rc`.

`Grunt` запускает установку пакетов при выполение тасков `default` и `build`. Инсталлируются все пакеты из `bower.json` не установленные ранее.

- Шаблонизация `twig`.
- CSS-фреймворк `bootstrap`.
- Иконоки `font-awesome`.
- JS-библиотеки `jquery` и `jqueryUI(используется slider)`.

# Структура проекта

- data `// Файлы данных YAML`
- images
- vendor `// установленные пакеты bower`
- js `// Файлы с префиксом test можно выкинуть сразу. Это генерация тестового контента и функционал "на показать"`
- less `// В корне лежат основные файлы стилей, раскладка, миксины, переменные`
    - components `// Перекрытые стили компонентов bootstrap`
    - partials `// Стили блоков и виджетов`
- views `// В корне шаблоны страниц`
    - layout `// Шаблоны раскладок`
    - components `// Шаблоны компонентов bootstrap`
    - partials `// Переиспользуемые шаблоны блоков и виджетов`

# Ссылки на документацию

- awesome-bootstrap-checkbox [http://flatlogic.github.io/awesome-bootstrap-checkbox/demo/](http://flatlogic.github.io/awesome-bootstrap-checkbox/demo/)
- bootstrap-fileinput [http://plugins.krajee.com/file-input](http://plugins.krajee.com/file-input)
- bootstrap-select [http://silviomoreto.github.io/bootstrap-select/](http://silviomoreto.github.io/bootstrap-select/)