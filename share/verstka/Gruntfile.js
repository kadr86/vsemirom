/*global module:false*/
module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        banner: '/* <%= pkg.title || pkg.name %> v<%= pkg.version %> */',
        jsFile: 'build/js/<%= pkg.name %>',
        // Task configuration.
        copy: {
            main: {
                files: [
                    {expand: true, src: 'images/**/*', dest: 'build/', filter: 'isFile'},
                    {expand: true, cwd: 'vendor/bootstrap/fonts/', src: ['**'], dest: 'build/fonts/glyphicons', filter: 'isFile'},
                    {expand: true, cwd: 'vendor/font-awesome/fonts/', src: ['**'], dest: 'build/fonts/font-awesome', filter: 'isFile'},
                    {expand: true, cwd: 'vendor/bootstrap-fileinput/img/', src: ['**'], dest: 'images', filter: 'isFile'}
                ]
            }
        },
        connect: {
            dev: {
                options: {
                    base: ['build', 'vendor'],
                    port: '5000',
                    host: '*'
                }
            }
        },
        clean: {
            build: {
                src: ['build/']
            }
        },
        concat: {
            options: {
                banner: '<%= banner %>',
                stripBanners: true,
                separator: '\n;'
            },
            dist: {
                src: [
                    'vendor/jquery/dist/jquery.min.js',
                    'vendor/jquery-ui/jquery-ui.min.js',
                    'vendor/bootstrap/dist/js/bootstrap.min.js',
                    'vendor/bootstrap-select/dist/js/bootstrap-select.min.js',
                    'vendor/bootstrap-select/dist/js/i18n/defaults-ru_RU.min.js',
                    'vendor/bootstrap-fileinput/js/fileinput.min.js',
                    'vendor/bootstrap-fileinput/js/fileinput_locale_ru.js',
                    'js/main.js',
                    'js/map.js',
                    'js/testFilterByLocation.js',
                    'js/testFilterByNeeds.js'
                ],
                dest: '<%= jsFile %>.js'
            }
        },
        uglify: {
            options: {
                banner: '<%= banner %>',
                sourceMap: true
            },
            dist: {
                src: '<%= jsFile %>.js',
                dest: '<%= jsFile %>.min.js'
            }
        },
        "bower-install-simple": {
            options: {
                color: true,
                directory: "vendor"
            },
            "prod": {
                options: {
                    production: true
                }
            },
            "dev": {
                options: {
                    production: false
                }
            }
        },
        twigger: {
            options: {
                twig: {
                    base: 'views'
                }
            },
            your_target: {
                files: [
                    {
                        data: 'data/test.yaml',
                        expand: true,
                        cwd: 'views',
                        src: ['**/*.twig', '!layout/**/*.twig', '!partials/**/*.twig', '!components/**/*.twig'], // Match twig templates but not partials
                        dest: 'build',
                        ext: '.html'
                    }
                ]
            }
        },
        less: {
            production: {
                options: {
                    paths: ['less'],
                    compress: true,
                    sourceMap: true,
                    plugins: [ new (require('less-plugin-autoprefix'))({browsers: [ 'last 5 versions' ]}) ]
                },
                files: [
                    {
                        src: ['less/main.less'],
                        dest: 'build/css/<%= pkg.name %>.css'
                    }
                ]
            }
        },
        watch: {
            options: {
                livereload: true
            },
            configFiles: {
                files: [ 'Gruntfile.js'],
                tasks: ['build'],
                options: {
                    reload: true
                }
            },
            data: {
                files: [ 'data/*.yaml'],
                tasks: ['build']
            },
            js: {
                files: ['js/**/*.js'],
                tasks: ['javascript']
            },
            css: {
                files: ['less/**/*.{less, css}'],
                tasks: ['less']
            },
            html: {
                files: ['views/**/*.twig'],
                tasks: ['twigger']
            }
        }
    });

    // These plugins provide necessary tasks.

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('javascript', ['bower-install-simple', 'concat', 'uglify']);
    grunt.registerTask('build', ['clean', 'copy', 'javascript', 'less', 'twigger']);
    grunt.registerTask('default', ['build', 'connect', 'watch']);

};
