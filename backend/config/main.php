<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'layout'    => 'main.twig',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'defaultRoute' => 'site/index',
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',

    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'user' => [
            'identityClass' => 'backend\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'view' => [
            'renderers' => [
                'defaultExtension' => 'twig',
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    // set cachePath to false in order to disable template caching
                    'cachePath' => '@runtime/Twig/cache',
                    // Array of twig options:
                    'options' => [
                        'auto_reload' => true,
                        'debug' => true,
                    ],

                    'globals' => [

                        'yii'         => 'Yii',
                        'app'         => 'Yii::$app',

                        'html'        => '\yii\helpers\Html',
                        'url'         => '\yii\helpers\Url',

                        'nav'         => 'yii\bootstrap\Nav',
                        'navBar'      => 'yii\bootstrap\NavBar',
                        'modal'       => 'yii\bootstrap\Modal',

                        'breadcrumbs' => 'yii\widgets\Breadcrumbs',
                        'aForm'       => 'yii\widgets\ActiveForm',
                        'maskedInput' => 'yii\widgets\MaskedInput',

                        'appAsset'    => 'backend\assets\AppAsset',
                    ],
                    'uses' => [
                        'yii\bootstrap'
                    ],
                    // ... see ViewRenderer for more options
                ],
            ],
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

//        'assetManager' => [
//            'converter' => [
//                'class' => 'yii\web\AssetConverter',
//                'commands' => [
//                    'less' => ['css', 'less {from} ../less_c/{to} --no-color'],
//                    //'ts'   => ['js', 'tsc --out {to} {from}'],
//                ],
//            ],
//        ],
        'session' => [
            'class' => 'yii\web\DbSession',
        ],

        'i18n' => [
            'translations' => [
                '*' => [
                    'sourceLanguage' => 'ru-RU',
                    'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@app/messages',
//                    'fileMap' => [
//                        'app' => 'app.php',
//                        'app/error' => 'error.php',
//                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];
