$(function(){

    var $progress = $('.progress-filter'),
        progressValUpdate = function (event, ui) {
            $(ui.handle).attr('data-progress', ui.value);
            $('#progress').val(ui.value);
            $('#progress')[0].form.onchange();
        };
    $progress.slider({
        range: "min",
        min: 0,
        max: 100,
        value: 0,
        create: function () {
            $progress.slider('value', $progress.data('progress'));
        },
        stop:progressValUpdate
        //change: progressValUpdate,
        //slide: progressValUpdate
    });
});