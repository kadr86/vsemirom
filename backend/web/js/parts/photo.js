function detectPhotoEditor() {
    $('input#gallery-file,input#photo-file').each(function () {
        var $this = $(this),
            isMultiple = $this.prop('multiple');

        $this
            .fileinput({
                allowedFileExtensions: ['jpg', 'jpeg', 'png'],
                allowedPreviewTypes: ['image'],
                previewFileType: 'image',
                previewFileIcon: '<i class="fa fa-file"></i>',
                previewSettings: {
                    image: {width: "100%", height: "auto"},
                    other: {width: "100%", height: "200px"}
                },
                overwriteInitial: !isMultiple,
                mainClass: 'photo-loader',
                layoutTemplates: {
                    main2: '<div class="{class}">\n{browse}\n{preview}\n</div>',
                    preview: '<div class="file-preview {class}">\n' +
                    '    <div class="kv-fileinput-error"></div>\n' +
                    '    <div class="{dropClass}">\n' +
                    '    <div class="file-preview-thumbnails clearfix">\n' +
                    '    </div>\n' +
                    '    <div class="file-preview-status text-center text-success"></div>\n' +
                    '    </div>\n' +
                    '</div>',
                    footer: '<div class="file-thumbnail-footer">\n{actions}\n</div>'
                },
                browseClass: (isMultiple ? 'btn btn-full btn-mid btn-info btn-light' : 'btn btn-full btn-primary btn-light'),
                browseLabel: '',
                browseIcon: (isMultiple ? '<i class="fa fa-camera fa-2x text-yellow"></i>' : '<i class="fa fa-camera fa-lg text-primary"></i>'),
                showCaption: false,
                showRemove: true,
                showUpload: false,
                showCancel: false,
                msgValidationErrorIcon: '<i class="fa fa-exclamation-circle"></i>',
                dropZoneEnabled: false,
                minFileCount: 1,
                maxFileCount: (isMultiple ? 0 : 1),
                uploadUrl: '/photo/upload?hash='+$this.data('photo-hash'),
                deleteUrl: '/photo/remove?id='+$this.data('photo-id'),
                //        uploadAsync:true,
                fileActionSettings: {
                    removeIcon: '<i class="fa fa-trash-o text-primary"></i>',
                    removeClass: 'btn btn-icon-round',
                    removeTitle: 'Удалить файл',
                    uploadIcon: '<i class="fa fa-upload text-primary"></i>',
                    uploadClass: 'btn btn-icon-round',
                    uploadTitle: 'Загрузить file',
                    indicatorNew: '<div class="btn btn-icon-round"><i class="fa fa-info-circle text-primary"></i></div>',
                    indicatorSuccess: '<div class="btn btn-icon-round"><i class="fa fa-check-circle file-icon-large text-success"></i></div>',
                    indicatorError: '<div class="btn btn-icon-round"><i class="fa fa-exclamation-circle text-danger"></i></div>',
                    indicatorLoading: '<div class="btn btn-icon-round"><i class="fa fa-spinner fa-pulse text-muted"></i></div>',
                    indicatorNewTitle: 'Файл еще не загружен',
                    indicatorSuccessTitle: 'Загружен',
                    indicatorErrorTitle: 'Ошибка загрузки',
                    indicatorLoadingTitle: 'Загрузка ...'
                }
            })
            .on('filebatchselected', function (event, files) {
                console.log(event);
                // trigger upload method immediately after files are selected
                $(this).fileinput("upload");
            });
    });

    $('.photo_editor').each(function () {
        var $this = $(this),
            previews = $this.data('preview').split(','),
            initialPreviews = [],
            initialPreviewConfig = [];

        for (var i = 0; i < previews.length; i++) {
            initialPreviews.push('<img src="' + previews[i] + '" class="file-preview-image">')
            initialPreviewConfig.push({
                caption: previews[i],
                url: '/photo/remove?id=' + $this.data('photo-id'),
                key: $this.data('photo-id')
            });
        }

        $this
            .fileinput({
                allowedFileExtensions: ['jpg', 'jpeg', 'png'],
                allowedPreviewTypes: ['image'],
                previewFileType: 'image',
                previewFileIcon: '<i class="fa fa-file"></i>',
                previewSettings: {
                    image: {width: "100%", height: "auto"},
                    other: {width: "100%", height: "200px"}
                },
                overwriteInitial: false,
                initialPreviewShowDelete: true,
                initialPreview: initialPreviews,
                initialPreviewConfig: initialPreviewConfig,
                mainClass: 'photo-loader',
                layoutTemplates: {
                    main2: '<div class="{class}">\n{preview}\n</div>{browse}\n',
                    preview: '<div class="file-preview {class}">\n' +
                    '    <div class="kv-fileinput-error"></div>\n' +
                    '    <div class="{dropClass}">\n' +
                    '    <div class="file-preview-thumbnails clearfix">\n' +
                    '    </div>\n' +
                    '    <div class="file-preview-status text-center text-success"></div>\n' +
                    '    </div>\n' +
                    '</div>',
                    footer: '<div class="file-thumbnail-footer">\n{actions}\n</div>'
                },
                browseClass: '',
                browseLabel: '',
                browseIcon: '',
                showCaption: false,
                showRemove: true,
                showUpload: false,
                showCancel: false,
                msgValidationErrorIcon: '<i class="fa fa-exclamation-circle"></i>',
                dropZoneEnabled: false,
                minFileCount: 1,
                maxFileCount: 1,
                deleteUrl: '/photo/remove?id='+$this.data('photo-id'),
                fileActionSettings: {
                    removeIcon: '<i class="fa fa-trash-o text-primary"></i>',
                    removeClass: 'btn btn-icon-round',
                    removeTitle: 'Удалить файл',
                    indicatorNew: '<div class="btn btn-icon-round"><i class="fa fa-info-circle text-primary"></i></div>',
                    indicatorSuccess: '<div class="btn btn-icon-round"><i class="fa fa-check-circle file-icon-large text-success"></i></div>',
                    indicatorError: '<div class="btn btn-icon-round"><i class="fa fa-exclamation-circle text-danger"></i></div>',
                    indicatorLoading: '<div class="btn btn-icon-round"><i class="fa fa-spinner fa-pulse text-muted"></i></div>',
                    indicatorNewTitle: 'Файл еще не загружен',
                    indicatorSuccessTitle: 'Загружен',
                    indicatorErrorTitle: 'Ошибка загрузки',
                    indicatorLoadingTitle: 'Загрузка ...'
                }

            });
    })
}

$(detectPhotoEditor());