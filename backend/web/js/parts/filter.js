var _tagsSelect = 0;
function _tagsSelectAll()
{

    var elements = document.getElementsByClassName("tag_check");
    for(var i=0; i<elements.length; i++) {
        elements[i].checked = _tagsSelect == 0 ? true : false;
    }
    _tagsSelect = !_tagsSelect;

    refreshFilter();
}
function refreshFilter()
{
    $.post(
        $('#filter_form').attr("action"), // serialize Yii2 form
        $('#filter_form').serialize()
    )
        .done(function(result)
        {
            $('#filter_form').parent().html(result.message);
            $('#search_list').load('/search/result');
        })
        .fail(function() {
            console.log("server error");
            $('#popup_form').replaceWith('<button class="newType">Fail</button>').fadeOut()
        });

    return false;
}

function reloadFilter()
{
    alert('test');
}