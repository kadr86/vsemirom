$(function () {
    var $regionSelect = $('#region'),
        $areaSelect   = $('#subregion'),
        $citySelect   = $('#city');

    //$regionSelect.selectpicker('refresh');

    /**
     * Loading subregions
     */
    $regionSelect.on('change', function () {
        var currentRegion = $(this).val();
        $areaSelect.prop('disabled', true).empty().selectpicker('refresh');
        $citySelect.prop('disabled', true).empty().selectpicker('refresh');

        $.ajax({
            url: '/location/subregions',
            dataType: 'json',
            data: {
                regionid: currentRegion
            },
            success: function (regions) {

                for (var id in regions) {
                    $areaSelect.append('<option value="' + id + '">' + regions[id] + '</option>')
                }

                $areaSelect.prop('disabled', false).selectpicker('refresh');
            }
        });
    });

    /**
     * Loading cities
     */
    $areaSelect.on('change', function () {
        var currentSubRegion = $(this).val();
        var currentRegion    = $regionSelect.val();

        $citySelect.prop('disabled', true).empty().selectpicker('refresh');

        $.ajax({
            url: '/location/cities',
            dataType: 'json',
            data: {
                regionid   : currentRegion,
                subregionid: currentSubRegion
            },
            success: function (cities) {

                for (var id in cities) {
                    $citySelect.append('<option value="' + id + '">' + cities[id] + '</option>')
                }

                $citySelect.prop('disabled', false).selectpicker('refresh');
            }
        });
    });
});