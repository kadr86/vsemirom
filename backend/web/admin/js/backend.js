/**
 * Opening popup window
 *
 * @param url
 */
function myPopup(url)
{
	$('#modal').modal('show')
        .find('#modalContent')
        .load(url);
}

/**
 * Submiting popup
 *
 * 1) checking form
 * 2) serializing form and submiting it by ajax
 * 3) reloading element 'reload_id' with 'reload_url'
 *
 * @param reload_id
 * @param reload_url
 */
function mySubmitPopup(reload_id,reload_url)
{
    var form = $('#popup_form');
    form.off('submit'); // disabling previous handlers
    form.on('submit', function(event) { // and setting new one

        event.preventDefault();

        $.post(
            form.attr("action"), // serialize Yii2 form
            form.serialize()
        )
            .done(function (result) {
                form.parent().html(result.message);
                $('#modal').modal('hide');
                if (reload_id != "" && reload_url != "")
                    $(reload_id).load(reload_url);
            })
            .fail(function () {
                console.log("server error");
            });
        return false;
    });

    form.yiiActiveForm('submitForm'); // validating
    return false;
}

/**
 * Closing popup dialog
 *
 * @returns {boolean}
 */
function myExitPopup()
{
	$('#modal').modal('hide');
	return false;
}

/**
 * Loads editor for specified need
 *
 * if id is 0, loads editor for the new need
 *
 * @param id
 */
function editNeed(id)
{
    loadWithFade("#need_"+id,"/admin/need/edit?id="+id)
}

/**
 * Saves need
 *
 * After load viewer for saved need
 *
 * @param id
 * @param churchid
 * @returns {boolean}
 */
function saveNeed(id,churchid)
{
    var form_id = '#edit_form_'+id;
    var form = $(form_id);

    form.off('submit'); // disabling all previous handlers
    form.on('submit',function(e) { // and setting new one
        e.preventDefault();

        $.post(
            $(form_id).attr("action"), // serialize Yii2 form
            $(form_id).serialize()
        )
            .done(function (result) {
                if (id == 0) {
                    $("#needs").append(result); // already contain div tag
                    getNeed(0, churchid); // refreshing new button
                } else {
                    $("#need_" + id).html(result);
                }
            })
            .fail(function () {
                console.log("server error");
                getNeed(id, churchid);
            });

        return false;
    });

    form.yiiActiveForm('submitForm'); // validating

    return false;
}

/**
 * Loads need with id
 *
 * @note: loads "new need button" if id is 0
 *
 * @param id
 * @param churchid
 */
function getNeed(id,churchid)
{
    loadWithFade("#need_"+id,"/admin/need/get?id="+id+"&churchid="+churchid)
}

/**
 * Open editor for new need
 *
 * @param churchid
 */
function newNeed(churchid)
{
    loadWithFade("#need_0","/admin/need/edit?churchid="+churchid)
}

/**
 * Remove need from church
 *
 * @todo: add confirmation dialog
 *
 * @param id
 * @returns {boolean}
 */
function deleteNeed(id)
{
  $.post(
       "/admin/need/delete", // serialize Yii2 form
       "id="+id
    )
        .done(function(result)
        {
            $("#need_"+id).slideUp('fast',function() {
                $("#need_"+id).remove();
            });
        })
        .fail(function() {
            console.log("server error");
        });
    return false;
}

/**
 * Helper for ajax loading with fading result
 *
 * @param id
 * @param url
 */
function loadWithFade(id,url)
{
    $(id).slideUp('fast', function() {
        $(id).load(
            url,
            function() {
                $(id).slideDown('fast');
                detectPhotoEditor();

            }
        )
    });
}

/**
 * Searching by keywords
 *
 * @returns {boolean}
 */
function searchByKeyword()
{
    var keyword = $('#search_keywords').val();
    document.location = '/admin/search/keyword?str='+keyword;

    return false;
}
