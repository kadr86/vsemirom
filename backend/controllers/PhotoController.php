<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

use common\models\Church;
use common\models\Photo;

//
// TODO: Add authentification!
// TODO: add churchid
//
class PhotoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles'   => ['@'],
                        'allow'   => true,
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Photo list
     * (ajax)
     */
    public function actionIndex($churchid)  // church id!
    {
        return $this->renderAjax('/form/photos',[
            'photos' => Photo::find()
                    ->where( ['churchid' => $churchid ] )
                    ->all()
            ]);
    }

    /**
     * Upload photo
     * (ajax)
     */
    public function actionUpload()
    {
        $hash = Yii::$app->request->get('hash');

    	$model = new Photo();
	    $model->hash = $hash;

    	if (Yii::$app->request->isPost)
    	{
    		$dir = Yii::getAlias('@app/web/uploads');
	        $uploaded = false;

	        $file = UploadedFile::getInstance($model,'photo');

            if (!$file)
            {
                return $this->renderContent("File is not selected");
            }

			if($model->validate())
			{
                $filename = $this->randomName(32).'.'.$file->getExtension();
				$uploaded = $file->saveAs($dir . '/' .$filename  );
                $model->photo = $filename;

                $model->save();
                return true;
			} else
            {
                return $this->renderContent("File validation failed");
            }
    	}

        return $this->renderAjax('/popup/upload',[
        	"model" => $model,
           ]);
    }

    /**
     * Photo editor
     * (ajax)
     */
    public function actionEdit($id)   // photo id
    {
        // TODO: make photo editor
    }

    /**
     * Photo removing confirmation dialog
     * (ajax)
     */
    public function actionRemove($id) // photo id
    {
        $model = Photo::findOne($id);
        if (empty($model))
            throw new NotFoundHttpException("Not found");

        $model->delete();

        return true;
    }

    /**
     * Str_random originaly from Laravel
     *
     * @param int $length
     * @return string
     */
    protected function randomName($length = 32)
    {
        $string = '';
        while (($len = strlen($string)) < $length) {
            $size = $length - $len;
            $bytes = rand(0,99999);
            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }
        return $string;
    }
}