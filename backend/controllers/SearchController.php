<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\Pagination;

use backend\models\SearchFilter;

use common\models\Church;
use common\models\Need;
use common\models\Tag;
use common\models\Location;

/**
 * Class SearchController
 *
 * @note: render() is overloaded!
 * @note: all parameters like sorting order or page number stored in models/SearchFilter
 *
 * @todo: move access to something else
 *
 * @package backend\controllers
 */
class SearchController extends Controller
{
    // parameters for render
    protected $params = array();

    // instance of SearchFilter model
    protected $filter = 0;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles'   => ['@'],
                        'allow'   => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * Main page
     */
    public function actionIndex()
    {

        $this->setTags();
        $this->setFilter();
        $this->setSearchList();

        $this->setLocationBox();

        return $this->render("index.twig");
    }

    /**
     * Tag list
     *
     * @note: as tags list require SearchFilter this function here (instead of /tag/list
     *
     * @ajax
     */
    public function actionTags()
    {
        $this->setTags();
        $this->setFilter();

        return $this->render("/search/parts/filter/parts/tags.twig",true);
    }

    /**
     * Filter setter
     *
     * set filter and returns 'true'
     *
     * @todo: make it return search list and remove actionResult()
     *
     * @ajax
     *
     */
    public function actionFilter()
    {
        $this->setFilter();

        return true; // не понятно почему, но с ним не работает фильтрация по районам
    }

    /**
     * Search result (with applied filter)
     *
     * @ajax
     */
    public function actionResult()
    {
        $this->setTags();
        $this->setFilter();
        $this->setSearchList();

        return $this->render("parts/items.twig",true);
    }

    /**
     * Filter reset
     *
     * resets filter and redirect to main search page
     *
     * @return redirect to /search
     */
    public function actionReset()
    {
        $this->filter = new SearchFilter;
        $this->filter->resetFilter();

        return $this->redirect("/search");
    }

    /**
     * Setting keyword to filter and redirect user to main search page
     *
     * @param $str
     * @return \yii\web\Response
     */
    public function actionKeyword($str)
    {
        $this->filter = new SearchFilter;
        $this->filter->loadFromSession();
        $this->filter->setKeyword($str);
        $this->filter->saveToSession();

        return $this->redirect("/search");
    }

    //
    // Protected functions-helpers for controller
    //

    /**
     * attaches tag list to view
     */
    protected function setTags()
    {
        $this->attachParameter("tags",Tag::find()->all());
    }

    /**
     * updates searchfilter and attaches it to view
     *
     * @todo: refactoring required
     */
    protected function setFilter($keyword = '',$forceKeyword = false)
    {
        $this->filter = new SearchFilter;
        $this->filter->loadFromSession();   //
        $this->filter->fromRequest();       // Проблема как я понял в том, что данные беруться из сессии, и они не так часто обновляются.
        $this->filter->saveToSession();     //Зачем так было сделано не понятно.
//        $this->filter =  [
//            'filterType' => 1,
//            'sort' => 'default',
//            'page' => 0,
//            'keyword' =>'',
//            'churchType' => 0,
//            'progress' => 0,
//            'active' =>'',
//            'need_help' =>'',
//            'tagsChecked' => '',
//            'regionid' => 47,
//            'subregionid' => 0,
//            'cityid' => 0,
//            'quickOnly' => 'on'
//        ];
        $this->attachParameter("filter",$this->filter);
    }

    /**
     * main search function
     */
    protected function setSearchList()
    {

        $query = $this->filter->filterType == SearchFilter::FT_CHURCH ?
            Church::search($this->filter) :
            Need::search($this->filter);

//        die();
        $sort = $this->filter->getSort();

        //
        // sorting result
        //
        $sort->params = [$sort->sortParam => $this->filter->sort];
        $sort->getOrders(true);
        $query->orderBy($sort->orders);

        //
        // pagination for result
        //
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),'pageSize'=>10]);
        $pages->setPage($this->filter->page-1);

        $list = $query->offset($pages->offset)
                      ->limit($pages->limit)
                      ->all();

        //
        // attaching result
        //
        $this->attachParameter("filterType",$this->filter->filterType);
        $this->attachParameter("list",$list);
        $this->attachParameter("pages",$pages);

        // sort parameters
        $this->attachParameter("sort",$sort);
        $order = $sort->getAttributeOrders();
        $dir   = reset($order);
        $this->attachParameter("sort_param",key($order));
        $this->attachParameter("sort_dir",$dir == SORT_ASC ? 'asc' : 'desc');
    }

    /**
     * attaches regions/subregions/cities to view
     */
    protected function setLocationBox()
    {
        list ($regions,$subregions,$cities) = Location::getAllAsArrays(
            $this->filter->regionid,
            $this->filter->subregionid
        );
        $this->attachParameter("regions",$regions);
        $this->attachParameter("subregions",$subregions);
        $this->attachParameter("cities",$cities);
    }

    //
    // Controller helpers
    //

    /**
     * Attach parameter for upcoming render
     *
     * @param $key
     * @param $value
     */
    protected function attachParameter($key,$value)
    {
        $this->params[$key] = $value;
//        echo '<pre>'; print_r($this->params); echo '</pre>';
    }

    /**
     * Reset all parameters
     */
    protected function resetParameters()
    {
        $this->params = array();
    }

    /**
     * Helper for rendering templates
     *
     * @note: uses parameters, that have been attached before
     *
     * @param string $view
     * @param bool $ajax
     * @return string
     */
    public function render($view,$ajax=false)
    {
        return ($ajax == true) ? parent::renderAjax($view,$this->params) : parent::render($view,$this->params);
    }
}
