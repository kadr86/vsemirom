<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

use common\models\Location;

/**
 * Class LocationController
 *
 * Helper for location selector
 *
 * @package backend\controllers
 */
class LocationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles'   => ['@'],
                        'allow'   => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * Returns subregions for specified region
     *
     * @param $regionid
     * @return array
     */
    public function actionSubregions($regionid)
    {
        Yii::$app->response->format = 'json';

        return ArrayHelper::map(
            Location::subregions($regionid)->all(),
            'subregionid',
            'title');
    }

    /**
     * Returns cities for specified subregion
     *
     * @note: regionid required to, as there are many same subregion's for different regions
     *
     * @param $regionid
     * @param $subregionid
     * @return array
     */
    public function actionCities($regionid,$subregionid)
    {
        Yii::$app->response->format = 'json';
        return ArrayHelper::map(
            Location::cities($regionid,$subregionid)->all(),
            'cityid',
            'title');
    }
}