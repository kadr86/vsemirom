<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace backend\controllers;

use common\models\Location;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use common\models\Church;
use common\models\Tag;
use common\models\Photo;

//
// TODO: Add authentification!
//
class ChurchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles'   => ['@'],
                        'allow'   => true,
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Church viewer
     */
    public function actionIndex($id)
    {
        $model   = Church::findOne($id);

        return $this->render("index.twig",
            [
                "church"  => $model,
            ]);
    }

    /**
     * Church editor
     */
    public function actionEdit()
    {
        $id = Yii::$app->request->isPost ? Yii::$app->request->post("id") : Yii::$app->request->get("id");
        $model = new Church(['type'=>Church::CT_MONASTERY]);
        if ($id)
            $model = Church::findOne($id);

        if (Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                $model->save();

                // attaching photos
                Photo::moveToChurch(Yii::$app->request->post('photoHash'),$model->id);

                return $this->redirect("/church/?id=".$model->id);
            }
            // @todo: attach flash with error message here
        }

        list($regions,$subregions,$cities) = Location::getAllAsArrays($model->regionid,$model->subregionid);

        return $this->render("/church/index.twig",
            [
                "church"     => $model,
                "editor"     => true,
                "regions"    => $regions,
                "subregions" => $subregions,
                "cities"     => $cities,

                "photoHash"  => md5(rand())
            ]);
    }

    /**
     * Church removing confirmation dialog
     * (ajax)
     */
    public function actionRemove($id = 0)
    {
        $model = Church::findOne($id);

        if (empty($model))
            throw new NotFoundHttpException("Not found");

        if (Yii::$app->request->isPost) {
            $model->delete();
            return $this->redirect("/search/");
        }

        return $this->renderAjax("/modal/church_remove.twig",["church"=>$model]);
    }
}

