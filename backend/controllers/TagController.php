<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use common\models\Tag;


class TagController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles'   => ['@'],
                        'allow'   => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * Tag editor
     *
     * @param int $id
     * @return string
     */
    public function actionEdit($id=0)
    {
        $model = Tag::findOne($id);

        if (!$model)
            $model = new Tag;

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->save();

            return "";
        }

        return $this->renderAjax("/modal/tag_editor.twig",['tag'=>$model]);
    }

    /**
     * Remove tag with specified id
     *
     * @param $id
     * @return string
     * @throws \Exception
     */
    public function actionRemove($id)
    {
        $model = Tag::findOne($id);
        if (!$model)
            throw new NotFoundHttpException("Tag not found");

        if (Yii::$app->request->isPost)
            $model->delete();

        return $this->renderAjax("/modal/tag_remove.twig",['tag'=>$model]);
    }
}