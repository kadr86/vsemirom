<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace backend\controllers;

use common\models\Church;
use common\models\Need;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use backend\models\LoginForm;

/**
 * Default controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow'   => true,
                    ],
                    [
                        'actions' => ['error'],
                        'allow'   => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                    [
                        'actions' => ['index', 'oferta', 'sendmail', 'pay', 'success', 'error'],
                        'allow'   => true,

                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout='site.twig';
        Yii::$app->view->params['hideFooter'] = true;
        return $this->render('index',['message' => false]);
    }

    public function actionOferta()
    {
        return $this->render('oferta');
    }

    public function actionSendmail()
    {
        $fio = Yii::$app->request->post('fio');
        $email = Yii::$app->request->post('email');
        $theme = Yii::$app->request->post('theme');
        $phone = Yii::$app->request->post('phone');
        $message = Yii::$app->request->post('message');
        $to ='vinveg@mail.ru';
        $subject = "$fio отправил сообщение с сайта vsemmirom.com.";
        $message_full = "<b>ФИО:</b> $fio<br /><b>email:</b> $email<br /><b>Тема обращения:</b> $theme <br /><b>Телефон:</b> $phone<br /><b>Сообщение:</b><br /><br />$message";
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        $headers .= 'To:'.$to."\r\n";
        $headers .= 'From: vsemmirom@site.com'."\r\n";
        mail($to,$subject,$message_full,$headers);
        return $this->redirect('/?message_send=ok');
    }

    public function beforeAction($action)
    {
        // ...set `$this->enableCsrfValidation` here based on some conditions...
        // call parent method that will check CSRF if such property is true.
        if ($action->id === 'login') {
            # code...
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if (Yii::$app->request->getMethod() == 'POST' && $model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
            return Yii::$app->getResponse()->redirect(Yii::$app->getUrlManager()->createUrl('/search'));
        } else {
            Yii::$app->view->params['hideFooter'] = true;
            return $this->render('login.twig', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return Yii::$app->getResponse()->redirect(Yii::$app->getUrlManager()->createUrl('../site'));
//        return $this->goHome();
    }

    public function actionPay()
    {
        return $this->render('pay.php', []);
    }

    public function actionSuccess()
    {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: vsemirom@site.com' . "\r\n";
        $fio = Yii::$app->request->get('fio');
        if ( (Yii::$app->request->get('need_id') && Yii::$app->request->get('need_id') != 'all') && Yii::$app->request->get('amount'))
        {

            $need_id = Yii::$app->request->get('need_id');
            $need = Need::findOne($need_id);
            $church = Church::findOne($need->churchid);
            $need->collected = $need->collected + Yii::$app->request->get("amount");
            $need->save();


            $need_name = $need->name;
            $church_name = $church->name;
            mail('hr2amo015@yandex.ru','Был совершен платеж.',"Пользователь $fio пожертвовал на: $need_name, для: $church_name, сумму: ".Yii::$app->request->get("amount"),$headers);


            if (Yii::$app->request->get('email'))
            {
                $to = Yii::$app->request->get('email');
                $subject = "Платеж прошел успешно.";
                $message_full = "Ваш платеж принят. Благодарим вас за пожертвование.";

                mail($to,$subject,$message_full,$headers);
            }
        }
        elseif (Yii::$app->request->get('church_id') && Yii::$app->request->get('amount'))
        {
            $church = Church::findOne(Yii::$app->request->get('church_id'));
            $church_name =$church->name;
            mail('hr2amo015@yandex.ru','Был совершен платеж.',"Пользователь $fio пожертвовал на все нужны для: $church_name, сумму: ".Yii::$app->request->get("amount"),$headers);

            if (Yii::$app->request->get('email'))
            {
                $to = Yii::$app->request->get('email');
                $subject = "Платеж прошел успешно.";
                $message_full = "Ваш платеж принят. Благодарим вас за пожертвование.";

                mail($to,$subject,$message_full,$headers);
            }
        }
        return $this->render("success.php", []);
    }

    public function actionError()
    {
        return $this->render("fail.php");
    }

}
