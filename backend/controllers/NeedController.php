<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;

use common\models\Need;
use common\models\Tag;
use common\models\Photo;

//
// TODO: Add authentification!
//
class NeedController extends Controller
{


	public $need_obj;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles'   => ['@'],
                        'allow'   => true,
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    //
    // There is no view pages. Only ajax frames
    //
    // public function actionIndex($id)
    // {
    //     return false;
    // }

    /**
     * Delete need confirmation dialog
     * (ajax)
     */
    public function actionDelete()
    {
    	$id    = Yii::$app->request->isPost ? Yii::$app->request->post("id")       : Yii::$app->request->get("id");

    	if (Yii::$app->request->isPost)
    	{
    		$model = Need::findOne($id);
            if (!empty($model))
    		    $model->delete();

    		return true;
    	}

    	return $this->renderAjax("/popup/need_remove",["id"=>$id]);
    }

    /**
     * Need editor
     * (ajax)
     */
    public function actionEdit()
    {
    	$id       = Yii::$app->request->isPost ? Yii::$app->request->post("id")       : Yii::$app->request->get("id");
    	$churchid = Yii::$app->request->isPost ? Yii::$app->request->post("churchid") : Yii::$app->request->get("churchid");

    	if (!empty($id))
    		$need = Need::findOne($id);
    	else
    	{
    		if (empty($churchid))
    			throw new BadRequestHttpException('Churchid should be defined.');

    		$need = new Need();
    		$need->churchid = $churchid;
    	}

        if (empty($need))
            throw new BadRequestHttpException('Need not found');

    	$tags = Tag::find()->all();

        // marking checked tags
        foreach ($tags as $k => $v) {
            foreach ($need->tags as $tag) {
                if ($v->id == $tag->id) {
                    $tags[$k]->checked = true;
                    break;
                }
            }
        }

    	// saving need

    	if (Yii::$app->request->isPost)
    	{
    		$need->attributes = Yii::$app->request->post("Need");
            $need->is_quick = isset(Yii::$app->request->post("Need")['is_quick']) ? 'Y' : 'N';
    		$need->save();

            // attaching images
            Photo::moveToChurch(Yii::$app->request->post('photoHash'),$need->churchid,$need->id);

			return $this->redirect('/church?id='.$need->churchid);
//    		return $this->renderAjax("/church/viewer/parts/need_viewer.twig",
//    			[
//    				"item" => $need,
//    				"tags" => $tags,
//    				"notAjax"=> empty($id) // adding div with id if this is new 'need'
//    			]);
    	}


    	return $this->renderAjax("/church/viewer/parts/need_editor.twig",[
            "item"      => $need,
            "item_id"   => (string)((int)$need->id),
            "tags"      => $tags,
            "photoHash" => md5(rand())
        ]);
    }

    /**
     * Need viewer
     * return "new need" button, if need is not found
     * (ajax)
     */
    public function actionGet($id,$churchid)
    {
    	if (!empty($id))
    	{
	    	$need = Need::findOne($id);

	    	return $this->renderAjax("/church/viewer/parts/need_viewer.twig",["item"=>$need,"withDiv"=>false]);
	    } else
	    {
	    	return $this->renderAjax("/church/viewer/parts/need_new_btn.twig",["churchid"=>$churchid]);
	    }
    }
}