<style>
    .pay
    {
        margin: 0 auto;
        width: 60%;
        padding-top: 20%;
    }
    .pay form fieldset label
    {
        width: 40%;
    }
    .pay form fieldset input[type='text'], input[type='email'] {
        height: 33px;
        width: 40%;
    }
    .pay form fieldset
    {
        margin-bottom: 10px;
    }
    .pay form fieldset input[type='submit']
    {
        padding: 8px 40px 8px 40px;
        font-weight: bold;
    }
</style>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <title>Форма оплаты</title>
</head>
<body>
<div class="pay">
    <form action="https://demomoney.yandex.ru/eshop.xml" method="post">
        <!-- Обязательные поля -->
        <input name="paymentType" value="" type="hidden">
        <input name="shopId" value="151" type="hidden"/>
        <input name="scid" value="363" type="hidden"/>
        <input type="hidden" value="http://vsemmirom.loc/site/pay/" name="shopSuccessURL">
        <input type="hidden" value="http://vsemmirom.loc/site/pay/" name="shopFailURL">
        <fieldset>
            <label for="sum">Сумма пожертвования:</label>
            <input name="sum" id="sum" value="100" type="text"/>
        </fieldset>
        <fieldset>
            <label for="user">Номер пользователя:</label>
            <input name="customerNumber" id="user" value="100500" type="text"/>
        </fieldset>
        <fieldset>
            <label for="phone">Телефон:</label>
            <input name="cps_phone" id="phone" value="79110000000" type="text"/>
        </fieldset>
        <fieldset>
            <label for="email">Email:</label>
            <input name="cps_email" id="email" value="user@domain.com" type="email"/>
        </fieldset>
        <fieldset align="right" style="padding-right: 19%">
            <input type="submit" value="Оплатить">
        </fieldset>

    </form>
</div>
</body>
</html>