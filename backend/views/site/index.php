<?php
/* @var $this yii\web\View */

$this->title = 'Всем миром';

if (Yii::$app->request->get('message_send') == 'ok')
{
    echo "<script>
        alert('Сообщение отправлено.');
    </script>";
}?>

<div class="index">
    <div class='head'>
        <div class="menu">
            <ul>
                <li><a href="#">Блог</a></li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </div>
        <div class="text-block">
            <p class="title">Всем миром</p>
            <p class="small-text">Мобильное приложение для сбора пожертвований<br /> на реконструкцияю храмов и монастырей <br />Русской Православной Церкви</p>
            <a href="http://www.apple.com/" target="_blank" class="button">Скачать приложение в APP STORE</a>
        </div>
    </div>
<!--    <div class="sub-head">-->
<!--        <p class="title">-->
<!--            Свести вместе тех, кто хочет и может помочь делу <br />восстановления церквей и сами храмы.-->
<!--        </p>-->
<!--        <p class="small-text">Проект для не равнодушных людей,  желающих принять участие в благом делевосстановления и строительства</p>-->
<!--    </div>-->
    <div class="patriarh">
        <div class="photo">
            <img src="images/review_photo.png" alt="">
        </div>
        <div class="content">
            <p class="title">Патриарх Московский и всея Руси Кирилл</p>
            <p  class="quote"><img src="images/quote.png" alt=""></p>

            <p class="comment">Строительство храма - это всегда некий подвиг, и духовный, и материальный.<br />В строительстве Храма обнаруживается подлинное стремление людей<br />
                к общению с Богом. Вот почему в молитве на освящение храма и говорится,<br />что тем, кто построил храм, прощаются грехи. Подвиг возведения храма<br />
                действительно принимается Господом как жертва благоуханная, угодная,<br />потому что через созидание Божиего храма души человеческие обращаются<br />
                ко спасению.</p>
<!--            <p class="small-text">Проект “Всем миром” создан по благословению Управления делами Московской Патриархии, <br /> для неравнодушных людей, желающих принять участие-->
<!--            в благом деле восстановления <br /> и строительства Храмов.</p>-->
        </div>
    </div>
    <div class="review">


            <p class="title">
                Проект "Всем миром" объединяет тех, кто заинтересован <br /> в возраждении архитектурного наследия<br />  и духовной культуры нашей страны.
            </p>
            <p class="comment">
                По благословению митрополита Санкт-Петербургского и Ладожского Варсонофия,<br />управляющего делами Московской Патриархии РПЦ.
            </p>



    </div>
    <div class="services">
        <p class="title">
            Удобный сервис для поиска информации о храмах, <br /> монастырях, часовнях и их нуждах, искать можно по:
        </p>
        <ul class="icons">
            <li>
                <img src="images/map.png" alt="">
                <span>Карте</span>
            </li>
            <li>
                <img src="images/flag.png" alt="">
                <span>Региону</span>
            </li>
            <li  class="last">
                <img src="images/city.png" alt="">
                <span>Городу</span>
            </li>
        </ul>
        <p class="small-text">Каждый посетитель может посмотреть подробную информацию о заинтересовавшем его храме или монастыре.</p>
    </div>
    <div class="sections">
        <div class="black" id="black">
            <div class="container-dialog">
                <div class="close-dialog"><img src="images/close.png" alt=""></div>
                <div class="dialog">
                    <p class="small-text">Вы можете оказать помощь Храмам<br />
                        установив приложение на свой iPhone или iPad.</p>
                    <a href="http://www.apple.com/" target="_blank" class="button-dialog">Перейти в App Store и установить приложение</a>
                    <br />
                    <a href="/site/oferta" target="_blank" class="oferta">Договор оферты</a>
                    <p class="small-text">Приложение для устройств на базе Android<br />
                        в данный момент не доступно, находится в разработке.</p>
                    <br />
                    <div class="fotter-dialog">
                        <p class="small-text">Либо Вы можете оказать пожертвование связавшись<br />
                            с нами. В качестве темы укажите “Хочу сделать пожертвование”.</p>
                        <a href="#contacts" class="button-dialog contact-us">Напишите нам и сделайте пожертвование</a>
                    </div>
                </div>
            </div>
        </div>
        <p class="title">
            <img src="images/flash.png" alt="">
            Созданы разделы для срочной помощи храмам
        </p>
        <a class="button want-to-help" href="#black">Хочу оказать помощь</a>
        <div class="img">
            <div>
                <p class="title">Мы позаботились о целевом использовании средств</p>
                <p class="small-text">После сбора нужной суммы и по завершении работ (или на промежуточных этапах) все жертвователи смогут<br /> посмотреть фотоотчет о том,
                как были истрачены их деньги.</p>
            </div>
            <div class="spacer"></div>
            <div>
                <p class="title">Следите за дальнейшей судьбой храма</p>
                <p class="small-text">Получайте сведения о последующем использовании пожертвованных средств. Отчет об использовании пожертвований<br />
                можно найти в открытом доступе, <u>в нашем блоге</u> или получить на свой электронный адрес</p>
            </div>
        </div>
    </div>
    <div class="losung">
        <p class="title">Жертвуй от чистого сердца</p>
        <p class="small-text">Благотворители смогут пожертвовать, как небольшие суммы, так и серьезные средства.<br />
        Персональная информация жертвователей будет надежно защищена. Все сведения из нашей базы данных <br />
        подвергаются тщательной проверке.</p>
        <a class="button want-to-help" href="#black">Хочу отказать помощь</a>
        <div class="img">
            <p class="title">На каждой литургии в церкви звучит возглас:</p>
            <p class="liturgiya">"О создателях святого храма сего Господу помолимся!" <br /><span>На этот призыв мы отвечаем самой главной христьянской молитвой</span></p>
        </div>
    </div>
    <div class="contacts">
        <div class="form">
            <p class="title" id="contacts">Контакты</p>
            <form action="<?php echo Yii::$app->getUrlManager()->createUrl('site/sendmail') ?>" method="post">
               <input type="hidden" name="_csrf" value="<?php echo Yii::$app->request->getCsrfToken() ?>"/>
               <fieldset>
                <input type="text" class="form-input" placeholder="Ваше имя: *" name="fio">
                <input type="text" class="form-input" placeholder="Email: *" name="email">
               </fieldset>
               <fieldset>
                <select name="theme" id="" class="form-input">
                    <option value="">Выберите тему обращения</option>
                    <option value="Сделать пожертвование">Сделать пожертвование</option>
                    <option value="Сотрудничество">Сотрудничество</option>
                    <option value="Другое">Другое</option>
                </select>
                <input type="text" class="form-input" placeholder="Телефон: *" name="phone">
               </fieldset>
               <fieldset>
                   <textarea name="message" cols="121" rows="6" placeholder="Введите текст сообщения: *"></textarea>
               </fieldset>
                <input class="btn-submit" type="submit" value="Отправить">
            </form>
        </div>
    </div>
    <div class="footer">
        <div class="column1">
            <p class="title">Телефон, Email:</p>
            <p class="phone">+7 (874) 234 57 62</p>
            <p class="email"><a class="email" href="mail-to:info@vsemmirom.com">info@vsemmirom.com</a></p>
        </div>
        <div class="column2">
            <p class="title">Адрес:</p>
            <p class="adress">465923 Россия, г. Москва  </p>
            <p class="adress-2">Новорижское шоссе, д. 34, каб. 289</p>
        </div>

        <div class="column3">
            <p class="first"><a href="/site/oferta">Договор оферты</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Наш блог</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://vk.com">Группа Вконтакте</a></p>
        </div>
        <div class="column4"><img src="images/app_store.png" alt=""></div>
        <div class="clearfix"></div>
        <div class="bottom-line">

            <div class="tm">Copyright © 2016 Автономная некоммерческая организация «Информационный центр по восстановлению храмов и монастырей «Храмо-созидатель»</div>
        </div>
    </div>

<!--    <div class="site-index text-center">-->
<!--            <p><a class="btn btn-info text-uppercase" href="/admin/search">Страница поиска</a></p>-->
<!---->
<!---->
<!--    </div>-->
</div>



