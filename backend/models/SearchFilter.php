<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace backend\models;

use Yii;
use yii\data\Sort;
use common\models\SearchFilter as SearchFilterBase;

/**
 * Class SearchFilter
 *
 * Search filter implementation with loading/saving to session
 * and with helpers to loading attributes from request
 *
 * @package backend\models
 */
class SearchFilter extends SearchFilterBase
{
    const SESSION_NAME = 'SearchFilter';

    //
    // What we are searching for
    //
    const FT_CHURCH = 1;
    const FT_NEED   = 2;
    public $filterType     = SearchFilter::FT_CHURCH;

    //
    // Sort by
    //
    const SORT_DEFAULT  = 'default';
    public $sort        = self::SORT_DEFAULT;

    // page number
    public $page        = 0; // for pagination



    //
    // sort attributes
    //
    protected $sort_attributes = [
        SearchFilter::FT_CHURCH => [
            'attributes' => [
                SearchFilter::SORT_DEFAULT => [
                    'asc' => ['{{%church}}.name' => SORT_ASC],
                    'desc' => ['{{%church}}.name' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label_desc' => 'Сортировать по имени (я-а)',
                    'label_asc' => 'Сортировать по имени (а-я)',
                ],
                'progress' => [
                    'asc' => ['progress' => SORT_ASC],
                    'desc' => ['progress' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label_desc' => 'Сортировать по прогрессу (убывание)',
                    'label_asc' => 'Сортировать по прогрессу (возрастание)',
                ],
                'required' => [
                    'asc' => ['required' => SORT_ASC],
                    'desc' => ['required' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label_desc' => 'Сортировать по необходимой сумме (убывание)',
                    'label_asc' => 'Сортировать по необходимой сумме (возрастание)',
                ],
                'collected' => [
                    'asc' => ['collected' => SORT_ASC],
                    'desc' => ['collected' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label_desc' => 'Сортировать по собранной сумме (убывание)',
                    'label_asc' => 'Сортировать по собранной сумме (возрастание)',
                ],
            ],
        ],
        SearchFilter::FT_NEED => [
            'attributes' => [
                SearchFilter::SORT_DEFAULT => [
                    'asc' => ['{{%need}}.name' => SORT_ASC],
                    'desc' => ['{{%need}}.name' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label_desc' => 'Сортировать по имени (я-а)',
                    'label_asc' => 'Сортировать по имени(а-я)',
                ],
                'progress' => [
                    'asc' => ['progress' => SORT_ASC],
                    'desc' => ['progress' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label_desc' => 'Сортировать по прогрессу (убывание)',
                    'label_asc' => 'Сортировать по прогрессу (возрастание)',
                ],
                'required' => [
                    'asc' => ['required' => SORT_ASC],
                    'desc' => ['required' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label_desc' => 'Сортировать по необходимой сумме (убывание)',
                    'label_asc' => 'Сортировать по прогрессу (возрастание)',
                ],
                'collected' => [
                    'asc' => ['collected' => SORT_ASC],
                    'desc' => ['collected' => SORT_DESC],
                    'default' => SORT_DESC,
                    'label_desc' => 'Сортировать по собранной сумме (убывание)',
                    'label_asc' => 'Сортировать по собранной сумме (возрастание)',
                ],
            ],
        ]
    ];

    /**
     * Loading filter from session
     */
    public function loadFromSession()
    {
        Yii::$app->session->open();

        $this->attributes = Yii::$app->session->get(self::SESSION_NAME,array());

    }

    /**
     * Saving filter to session
     */
    public function saveToSession()
    {
        Yii::$app->session->set(self::SESSION_NAME,$this->getAttributes());
        Yii::$app->session->close();

    }

    /**
     * Loading from request
     */
    public function fromRequest()
    {
        $this->attributes = $this->getRequestVar('SearchFilter');
//        echo '<pre>'; print_r($this->attributes); echo '</pre>';
        if ($this->getRequestVar('forceTags') != null)
            $this->quickOnly = $this->getRequestVar('quickOnly');

        if ($this->getRequestVar("forceTags") != null)
            $this->tagsChecked = $this->getRequestVar('tagsChecked');

        // sort order
        $sort = $this->getRequestVar("sort");

        if ($sort !== false) {
            //
            // checking sort value
            //
            $tmpSort = $sort;
            if (strncmp($tmpSort, '-', 1) === 0) {
                $descending = true;
                $tmpSort = substr($tmpSort, 1);
            }

            if (!array_key_exists(
                $tmpSort,
                $this->sort_attributes[
                $this->filterType == self::FT_CHURCH ?
                    self::FT_CHURCH : self::FT_NEED
                ]['attributes']
            ))
                $sort = self::SORT_DEFAULT;

            // setting it
            $this->sort = $sort;
        }

        // detecting page
        $page = $this->getRequestVar("page");
        if ($page !== false)
            $this->page = $page;
    }

    /**
     * Assigning search by keyword
     *
     * @param $str
     */
    public function setKeyword($str)
    {
        $this->keyword = $str;
    }

    /**
     * Helper for getting var from request
     *
     * @param $name
     * @param bool $default
     * @return array|mixed
     */
    protected function getRequestVar($name,$default=false)
    {
        return Yii::$app->request->method == 'POST' ?
            Yii::$app->request->post($name,$default) :
            Yii::$app->request->get($name,$default);
    }

    /**
     * Resetting filter
     */
    public function resetFilter()
    {
        Yii::$app->session->open();
        Yii::$app->session->set(self::SESSION_NAME,array());
        Yii::$app->session->close();
    }

    /**
     * Getting sorter
     *
     * @return Sort
     */
    public function getSort()
    {
        return new Sort($this->sort_attributes[
            $this->filterType == self::FT_CHURCH ? self::FT_CHURCH : self::FT_NEED]);
    }
}