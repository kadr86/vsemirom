<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',

        'css/main.css',
        'css/main_page.css',

//        'less/base.less',
//        'less/layout.less',
//        'less/main.less',
//        'less/mixins.less',
//        'less/ssettings.less'
    ];
    public $js = [
        'js/backend.js',
        'js/jquery.scrollTo-min.js',

        'http://maps.google.com/maps/api/js?sensor=false'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\validators\ValidationAsset',
        'yii\widgets\ActiveFormAsset',
        'yii\bootstrap\BootstrapAsset',
        'kartik\file\FileInputAsset',
        'cakebake\bootstrap\select\BootstrapSelectAsset',
        'yii\jui\JuiAsset',
    ];

    public function addJS($file)
    {
        $this->js[] = $file;
    }
}
