<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace api\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Class Identity
 *
 * Dummy identity interface for rest
 *
 * @package api\models
 */
class DummyIdentity extends ActiveRecord implements IdentityInterface
{
}