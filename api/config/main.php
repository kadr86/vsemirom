<?php

/**
 * itech-mobile.ru
 * @author anton
 */

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'defaultRoute'        => 'help/index',

    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'user' => [
            'identityClass' => 'api\models\DummyIdentity',
            'loginUrl'      => null,
        ],
        'request'   => [
            'enableCsrfValidation' => false,
            'enableCookieValidation' => false
        ],
        // @todo: authentification via app key
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'errorHandler' => [
            'errorAction' => 'help/error',
        ],

    ],
    'params' => $params,
];
