<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace api\controllers;

use common\models\Tag;
use yii\rest\Controller;

/**
 * Class TagController
 *
 * return tags with identifiers
 *
 * @package api\controllers
 */
class TagController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * Reseting default rest actions
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }

    /**
     * Returning all tags
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function actionIndex()
    {
        return new \yii\data\ActiveDataProvider([
            'query' => Tag::find(),
        ]);
    }
}