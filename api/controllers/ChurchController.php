<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace api\controllers;

use yii\rest\ActiveController;

/**
 * Class ChurchController
 *
 * Churches list
 *
 * @package api\controllers
 */
class ChurchController extends ActiveController
{
    public $modelClass = 'common\models\Church';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * Available actions
     *
     * @return array
     */
    public function actions()
    {
        $actions = [
            'index' => [
                'class'      => 'api\controllers\actions\FilteredIndexAction',
                'modelClass' => 'common\models\Church',
                'sort'       => [
                    'name_asc'      => "{{%church}}.name asc",
                    'name_desc'     => "{{%church}}.name desc",
                    "progress_asc"  => "progress asc",
                    "progress_desc" => "progress desc"
                ]
            ]
        ];

        return $actions;
    }
}