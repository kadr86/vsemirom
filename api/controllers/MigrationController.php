<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace api\controllers;

use common\models\Location;
use yii\rest\ActiveController;

class MigrationController extends ActiveController
{
    public $modelClass = 'common\models\Church';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
    
    /**
     * controller actions
     */
    public function actions()
    {
        return [
            'church' => [
                'class'      => 'api\controllers\actions\MigrationAction',
                'modelClass' => 'common\models\Church',
            ]
        ];
    }
}