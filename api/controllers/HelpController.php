<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace api\controllers;

use \YandexMoney\API;
use \YandexMoney\ExternalPayment;
use \yii\web\Cookie;
use yii\bootstrap\Html;
use yii\rest\Controller;
//require_once (__DIR__.'../../constants.php');
define("CLIENT_ID", "96E2C166BCA7AE9FB43792713F2DAF8711642E076CF91752E01ADDC40168043F");
define('CLIENT_SECRET_WORD', '4E22E0E2EC62FF980735C0AA1B1E1063055AA43B99A83D9F488AE94AB550403414195FF48D26CA77DE54DE313B71D2E42251C9FF2606C372AF94FF3F9E412728');
define("JSON_OPTIONS", JSON_PRETTY_PRINT
    | JSON_HEX_TAG
    | JSON_HEX_QUOT
    | JSON_HEX_AMP
    | JSON_UNESCAPED_UNICODE);

class HelpController extends Controller
{

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['error']);
        unset($actions['getpayurl']);

        return $actions;
    }

    public function actionIndex()
    {
        return [
            "Help content"
        ];
    }

    public function actionError()
    {
        return [
            "Error occured. Please check your request."
        ];
    }

    /**
     * Функция возвращает ссылку на форму yandex оплаты через карту.
     * @return string
     */
    public function actionGetpayurl()
    {
//        $church_id = \Yii::$app->request->get('church_id');
//        if (empty($church_id)) return 'church_id is required, but not present';

        $phone = \Yii::$app->request->get('phone');
        $amount = \Yii::$app->request->get('amount');
        $need_id = \Yii::$app->request->get('need_id');
        if (empty($need_id) || empty($amount) || empty($phone)) return 'one of filed from [need_id, amount, phone] or more is required, but not present';

        $user_email = \Yii::$app->request->get('email');
        $comment = \Yii::$app->request->get('comment');
        $fio = \Yii::$app->request->get('name');
        $church_id = \Yii::$app->request->get('church_id');


        $base_path = \Yii::$app->request->getServerName();


        $instance_id_json = ExternalPayment::getInstanceId(CLIENT_ID);
        $expire = strtotime("+10 min");

        $options = [];
        $options['name'] = 'instance_id';
        $options['value'] = $instance_id_json->instance_id;
        $options['expire'] = $expire;
        $options['path'] = '/';

        $instance_id_cookie = new Cookie($options);
        \Yii::$app->response->cookies->add($instance_id_cookie);

        $options = [];
        $options['name'] = 'result/instance_id';
        $options['value'] = json_encode($instance_id_json);
        $options['expire'] = $expire;
        $options['path'] = '/';

        $result_instance_id_cookie = new Cookie($options);
        \Yii::$app->response->cookies->add($result_instance_id_cookie);

        $instance_id = $instance_id_json->instance_id;
        $api = new ExternalPayment($instance_id);
        // check response

        $request_result = $api->request(array(
            "pattern_id" => "phone-topup",
            "phone-number" => \Yii::$app->request->get('phone'),
            "amount" => \Yii::$app->request->get('amount')
        ));
        if($request_result->status != "success") {
            $params = array(
                "text" => json_encode($request_result, JSON_OPTIONS),
                "home" => "../"
            );
            return $params;
        }
        $options["request_id"] = $request_result->request_id;

        $host = \Yii::$app->request->getServerName().':'.\Yii::$app->request->getServerPort();
        $base_path =
            "http://"
            . $host
            . \Yii::$app->request->getScriptFile()
            . "..";
        $process_result = $api->process(array(
            "request_id" => $request_result->request_id,
            "ext_auth_success_uri" => $base_path . "/site/success/?need_id=$need_id&amount=$amount&phone=$phone&email=$user_email&comment=$comment&fio=$fio&church_id=$church_id",
            "ext_auth_fail_uri" => $base_path . "/site/error/"
        ));

        $options = [];
        $options['name'] = 'result/request';
        $options['value'] = json_encode($request_result);
        $options['expire'] = $expire;
        $options['path'] = '/';

        $result_request_cookie = new Cookie($options);
        \Yii::$app->response->cookies->add($result_request_cookie);

        $options = [];
        $options['name'] = 'result/process';
        $options['value'] = json_encode($process_result);
        $options['expire'] = $expire;
        $options['path'] = '/';

        $result_process_cookie = new Cookie($options);
        \Yii::$app->response->cookies->add($result_process_cookie);

        $url = sprintf("%s?%s", $process_result->acs_uri,
            http_build_query($process_result->acs_params));

        return $url;
    }


}