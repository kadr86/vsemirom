<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace api\controllers\actions;

use Yii;
use common\models\SearchFilter;
use yii\rest\IndexAction;

/**
 * Class FilteredIndexAction
 *
 * using SearchFilter model for filtering
 *
 * @package api\controllers\actions
 */
class FilteredIndexAction extends IndexAction
{
    public $sort = [];

    /**
     * Our vision of preparing data provider
     *
     * @return mixed|\yii\data\ActiveDataProvider
     */
    protected function prepareDataProvider()
    {
        if ($this->prepareDataProvider !== null) {
            return call_user_func($this->prepareDataProvider, $this);
        }

        $filter = new SearchFilter();

        // filters
        if ($this->isVar('type'))     $filter->churchType  = $this->getVar('type');
        if ($this->isVar('keyword'))  $filter->keyword     = $this->getVar('keyword');
        if ($this->isVar('progress')) $filter->progress    = $this->getVar('progress');
        if ($this->isVar('active')) $filter->active    = $this->getVar('active');
        if ($this->isVar('need_help')) $filter->need_help    = $this->getVar('need_help');
        if ($this->isVar('tags'))     $filter->setTags($this->getVar('tags'));
        if ($this->isVar('regionid')) {
            $filter->regionid    = $this->getVar('regionid');
            if ($this->isVar('subregionid')) {
                $filter->subregionid = $this->getVar('subregionid');
                if ($this->isVar('cityid'))
                    $filter->cityid = $this->getVar('cityid');
            }
        }

        /* @var $modelClass \yii\db\BaseActiveRecord */
        $modelClass = $this->modelClass;

        $query = $modelClass::search($filter);
        if ($this->getSort() != false)
            $query->orderBy($this->getSort());

        return new \yii\data\ActiveDataProvider([
            'query' => $query,
        ]);
    }

    /**
     * Helper for checking if variable assigned to request
     *
     * @param $name
     * @return bool
     */
    protected function isVar($name)
    {
        return $this->getVar($name) !== false;
    }

    /**
     * Getting variable from request
     *
     * @param $name
     * @param bool $default
     * @return array|mixed
     */
    protected function getVar($name,$default = false)
    {
        return Yii::$app->request->get($name,$default);
    }

    /**
     * Getting sort expression
     *
     * @return bool|mixed
     */
    protected function getSort()
    {
        if (empty($this->sort))
            return false;

        $sort = $this->getVar("sort");
        if (isset($this->sort[$sort]))
            return $this->sort[$sort];

        return reset($this->sort);
    }
}