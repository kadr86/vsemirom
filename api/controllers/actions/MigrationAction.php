<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace api\controllers\actions;

use common\models\Church;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\rest\Action;

/**
 * Class MigrationAction
 *
 * simple migration for tables, that uses TimestampBehaviour and SoftDelete
 *
 * @package api\controllers\actions
 */
class MigrationAction extends \yii\base\Action
{
    public $modelClass = '';

    /**
     * @return ActiveDataProvider
     */
    public function run()
    {
        return $this->prepareDataProvider();
    }

    /**
     * Our vision of preparing data provider
     *
     * @return mixed|\yii\data\ActiveDataProvider
     */
    protected function prepareDataProvider()
    {
        $timestamp = $this->isVar('timestamp') == true ? $this->getVar('timestamp') : 0;
        $timestamp_parameter = new Expression('FROM_UNIXTIME(:time)',[':time'=>$timestamp]);

        $class = $this->modelClass;

        $query = $class::find();
        $query->orWhere(['>','create_time',$timestamp_parameter]);
        $query->orWhere(['>','update_time',$timestamp_parameter]);
        $query->orWhere(['>','delete_time',$timestamp_parameter]);

        return new \yii\data\ActiveDataProvider([
            'query' => $query,
        ]);
    }

    /**
     * Helper for checking if variable assigned to request
     *
     * @param $name
     * @return bool
     */
    protected function isVar($name)
    {
        return $this->getVar($name) !== false;
    }

    /**
     * Getting variable from request
     *
     * @param $name
     * @param bool $default
     * @return array|mixed
     */
    protected function getVar($name,$default = false)
    {
        return Yii::$app->request->get($name,$default);
    }
}