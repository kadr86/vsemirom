<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace api\controllers;

use common\models\Location;
use yii\rest\Controller;

/**
 * Class LocationController
 *
 * return internal location names and identifiers
 *
 * @package api\controllers
 */
class LocationController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * Overriding default rest requests
     *
     * @return array
     */
    public function actions()
    {
        return [];
    }

    /**
     * Returning regions
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function actionRegions()
    {
        return new \yii\data\ActiveDataProvider([
            'query' => Location::regions(),
        ]);
    }

    /**
     * Returning subregions
     *
     * @param $regionid
     * @return \yii\data\ActiveDataProvider
     */
    public function actionSubregions($regionid)
    {
        return new \yii\data\ActiveDataProvider([
            'query' => Location::subregions($regionid),
        ]);
    }

    /**
     * Returning cities
     *
     * @param $regionid
     * @param $subregionid
     * @return \yii\data\ActiveDataProvider
     */
    public function actionCities($regionid=null,$subregionid=null)
    {
        return new \yii\data\ActiveDataProvider([
            'query' => Location::cities($regionid,$subregionid),
        ]);
    }
}
