<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace api\controllers;

use yii\rest\Controller;

/**
 * Class NeedController
 *
 * needs list
 *
 * @package api\controllers
 */
class NeedController extends Controller
{
    public $modelClass = 'common\models\Need';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * Available actions
     *
     * @return array
     */
    public function actions()
    {
        $actions = [
            'index' => [
                'class'      => 'api\controllers\actions\FilteredIndexAction',
                'modelClass' => 'common\models\Need',
                'sort'       => [
                    'name_asc'      => "{{%need}}.name asc",
                    'name_desc'     => "{{%need}}.name desc",
                    "progress_asc"  => "progress asc",
                    "progress_desc" => "progress desc"
                ]
            ]
        ];

        return $actions;
    }
}