<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace common\components;

trait ChangeLog
{
    public $change_type = '';
    public $change_time = 0;

    protected function updateChangelogFields()
    {
        $c = new \DateTime($this->create_time);
        $u = new \DateTime($this->update_time);
        $d = new \DateTime($this->delete_time);

        if ($d->getTimestamp() > $u->getTimestamp() && $d->getTimestamp() > $u->getTimestamp()) {
            $this->change_type = 'deleted';
            $this->change_time = $d->getTimestamp();
        }
        elseif ($u->getTimestamp() > $c->getTimestamp()) {
            $this->change_type = 'updated';
            $this->change_time = $u->getTimestamp();
        }
        else {
            $this->change_type = 'created';
            $this->change_time = $c->getTimestamp();
        }
    }

    protected function getChangelogFields()
    {
        return ['change_type','change_time'];
    }
}