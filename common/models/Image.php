<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace common\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;
use common\models\Category;

/**
 * Class Image
 *
 * @deprecated
 * @todo: remove this class
 *
 * @package common\models
 */
class Image extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%image}}';
    }

    // validation
    public function rules()
    {
        return [
        ];
    }

    public function category()
    {
        return $this->hasOne(Category::className(),["id" => "category_id"]);
    }
}

