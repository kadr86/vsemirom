<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace common\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;
use common\models\Image;

/**
 * Class Category
 *
 * @deprecated
 * @todo: remove this class
 *
 * @package common\models
 */
class Category extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%category}}';
    }

    // validation
    public function rules()
    {
        return [];
    }

    public function image()
    {
        return $this->hasOne(Image::className(),['id'=>'category_id']);
    }
}

