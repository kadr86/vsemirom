<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace common\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * Class Photo
 *
 * @todo: delete file after deleting model
 *
 * @package common\models
 */
class Photo extends ActiveRecord
{
    /**
     * Adding 'url' field for helping to find the foto
     *
     * @return array
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields[] = 'url';

        return $fields;
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            ['photo','file','extensions'=> ['jpg','jpeg','png']],

        ];
    }

    /**
     * Moving temporarily uploaded photos to church
     *
     * @todo: delete photos, that was uploaded long time ago and not assigned
     *
     * @param $hash
     * @param $churchid
     * @param int $needid
     */
    public static function moveToChurch($hash,$churchid,$needid=0)
    {
        Photo::updateAll(['churchid'=>$churchid,'needid'=>$needid],['hash'=>$hash]);
    }

    /**
     * Helper for finding photo file
     *
     * @todo: add host to the url
     * @
     * @return string
     */
    public function getUrl()
    {
        return 'uploads/'.$this->photo;
    }

}