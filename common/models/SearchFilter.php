<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace common\models;

use Yii;
use yii\base\Model;
use common\models\Church;

/**
 * Class SearchFilter
 *
 * Helper model for the search filter
 *
 * @note: tagsChecked is array with elements like tagid=>'on'
 *
 * @package backend\models
 */
class SearchFilter extends Model
{
	//
	// Search keyword
	//
	public $keyword        = "";

	//
	// Church type filter
	//
	public $churchType     = Church::CT_NULL;

	// 
	// Progress filter
	//
	public $progress     = 0;

    //
    //Search active church
    //
    public $active;

    //
    //Search church where Need quick help
    //
    public $need_help;

	//
	// Array with ids of checked tags
	//
	public $tagsChecked = array();

	// Location filter
	public $regionid    = 0;
	public $subregionid = 0;
	public $cityid      = 0;

    public $quickOnly   = 0;

	/**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
        	["filterType","default","value"=>1],
        	[["filterType","churchType","progress"],"integer"],

        	["keyword","string","max"=>50],
        	["progress", "integer"],

        	[["tagsChecked","regionid","subregionid","cityid"],"safe"],

            ['page','integer'],
            ['sort','string'],
            ['quickOnly','safe']
        ];
    }

    /**
     * Setting tags
     *
     * @param $tags (string with ',' delimiter)
     */
    public function setTags($tags)
    {
        if (empty($tags))
            return;

        $this->tagsChecked = array_fill_keys(explode(',',htmlspecialchars_decode($tags)),'on');
    }
}