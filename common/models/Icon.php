<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\imagine\Image;

/**
 * Class Icon (extends from photo)
 *
 * @package common\models
 */
class Icon extends ActiveRecord
{
    protected $thumbUrl = '';

    const T_WIDTH  = 60;
    const T_HEIGHT = 60;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%photo}}';
    }

    /**
     * Adding 'url' field for helping to find the foto
     *
     * @return array
     */
    public function fields()
    {
        return ['url'];
    }

    /**
     * Thumbnailing
     */
    public function afterFind()
    {
        $return = parent::afterFind();

        $this->generateThumbnail();

        return $return;
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Helper for finding photo file
     *
     * @todo: add host to the url
     * @
     * @return string
     */
    public function getUrl()
    {
        return 'uploads/'.$this->thumbUrl;
    }

    /**
     * generating thumbnail
     */
    protected function generateThumbnail()
    {
        list($file,$ext) = $this->parseName();

        if (empty($file))
            $this->thumbUrl = $this->photo;

        $this->thumbUrl = $file.'_thumb.'.$ext;

        if (!file_exists(Yii::getAlias('@backend/web/uploads').'/'.$this->thumbUrl)) {
            Image::thumbnail(Yii::getAlias('@backend/web/uploads').'/'.$this->photo,self::T_WIDTH,self::T_HEIGHT)
                ->save(Yii::getAlias('@backend/web/uploads').'/'.$this->thumbUrl);
        }
    }

    /**
     * @return array
     */
    protected function parseName()
    {
        if (!preg_match('/([^\.]+)\.([^\.]+)/',$this->photo,$matches))
            return ['',''];

        return [$matches[1],$matches[2]];
    }
}