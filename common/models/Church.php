<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace common\models;

use common\components\ChangeLog;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use amnah\yii2\behaviors\SoftDelete;
use common\models\Photo;
use common\models\Need;
use common\models\Tag;

/**
 * Class Church
 *
 * Church model
 *
 * @package common\models
 */
class Church extends ActiveRecord
{
    use ChangeLog;

    //
    // church types
    //
	const CT_NULL   = 0;
	const CT_MONASTERY = 1;
	const CT_CHURCH = 2;
	const CT_CHAPEL = 3;

    //
    // dynamic fields
    // @todo: check if they are @deprecated
    //
    public $required  = 0;
    public $collected = 0;
    public $progress  = 0;


    /**
     * @inheritdoc
     */
	public static function tableName()
    {
        return '{{%church}}';
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100],
            [['type'], 'in', 'range' => [Church::CT_MONASTERY,Church::CT_CHURCH,Church::CT_CHAPEL]],
            [['email'], 'email'],
            [['url'], 'url', 'enableIDN' => true],
            [['description'], 'string'],
            [['owner'], 'string','max' => 255],
            [['phone'], 'string','max' => 20],

            // location
            [['regionid','subregionid','cityid'],'number'],

            // requisites
            [[
                'req_address',
                'req_phone',
                'req_fax',
                'req_rs',
                'req_ks',
                'req_bik',
                'req_inn',
                'req_kpp',
                'req_ogrn',
                'req_okved',
                'req_okpo',
                'need_help',
            ],'safe'],

            // info
            [[
                'info_history',
                'info_build',
                'info_count',
                'info_sacred'
            ],'safe'],

            [['gapi_lat','gapi_lng'],'double'],

            [['name','type', 'active'], 'required']
        ];
    }

    /**
     * Labels for columns
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'description' => 'Описание',
            'url' => 'Сайт',
            'owner' => 'Настоятель',
            'phone' => 'Телефон',
            'active' => 'Активность',
            'need_help' => 'Требуется срочная помощь',
        ];
    }

    /**
     * Additional fields, that can be expanded
     *
     * @return array
     */
    public function extraFields()
    {
        return ['photos','tags','needs'];
    }

    /**
     * Additional fields
     *
     * @return array
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields[] = 'region';
        $fields[] = 'subregion';
        $fields[] = 'city';
        $fields[] = 'icon';

        return array_merge($fields,$this->getChangelogFields());
    }

    /**
     * Converting locations to string for mobile app
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->updateChangelogFields();
    }

    /**
     * Behaviors of model
     *
     * uses TimestampBehavior and SoftDelete
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SoftDelete::className(),
                'attribute' => 'delete_time',
                'value' => new Expression('NOW()'),
                'safeMode' => true, // this processes '$model->delete()' calls as soft-deletes
            ]
        ];
    }

    /**
     * @return static
     */
    public static function find()
    {
        return parent::find()->where(['delete_time' => null]);
    }

    /**
     * Relation to photos
     *
     * @note: photos for church 'need' will be here too
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
    	return $this->hasMany(Photo::className(),['churchid'=>'id'])->andWhere(['needid'=>0]);
    }

    /**
     * Relation to the church icon
     *
     * @return static
     */
    public function getIcon()
    {
        return $this->hasOne(Icon::className(),['churchid'=>'id'])->andWhere(['needid'=>0]);
    }

    /**
     * Relation to needs
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNeeds()
    {
        return $this->hasMany(Need::className(),['churchid'=>'id']);
    }

    public function getRegion()
    {
        return $this->hasOne(Location::className(),['regionid'=>'regionid']);
    }

    public function getSubregion()
    {
        return $this->hasOne(Location::className(),['regionid'=>'regionid','subregionid'=>'subregionid']);
    }
    public function getCity()
    {
        return $this->hasOne(Location::className(),['regionid'=>'regionid','subregionid'=>'subregionid','cityid'=>'cityid']);
    }

    /**
     * Relation to tags
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getTags()
    {
        $tags = Tag::find();
        $tags->select("{{%tag}}.*,COUNT({{%need_tag}}.id) as needCount");
        $tags->from("{{%tag}}");
            $tags->innerJoin("{{%need}}","{{%need}}.churchid=:id",[":id"=>$this->id]);
            $tags->innerJoin("{{%need_tag}}","{{%need_tag}}.tagid={{%tag}}.id AND {{%need_tag}}.needid={{%need}}.id");
        $tags->groupBy("{{%tag}}.id");
        return $tags->all();
    }

    /**
     * Searching by filter
     *
     * @param $filter \common\models\SearchFilter
     *
     * @return Query
     */
    public static function search($filter)
    {
        $query = Church::find();
        $query->select([
            "{{%church}}.*",
            "SUM({{%need}}.required) as required",
            "SUM({{%need}}.collected) as collected",
            "SUM({{%need}}.collected)*100/SUM({{%need}}.required) as progress"
        ]);

        if (!empty($filter->tagsChecked) || !empty($filter->quickOnly))
        {
            $query->innerJoin("{{%need}}","{{%need}}.churchid={{%church}}.id");
        } else
        {
            $query->leftJoin("{{%need}}","{{%need}}.churchid={{%church}}.id");
        }

        if (!empty($filter->tagsChecked)) {
            $query->innerJoin("{{%need_tag}}","{{%need_tag}}.needid={{%need}}.id");
        }

        // search keyword condition
        if (!empty($filter->keyword))
            $query->andFilterWhere(['or',
                    ['like','{{%church}}.name',$filter->keyword],
                    ['like','{{%church}}.description',$filter->keyword]
                ]
            );

        $query->groupBy("{{%church}}.id");

        if ($filter->churchType != Church::CT_NULL)
            $query->andWhere("type=:type",[":type"=>$filter->churchType]);

        // tags filter
        if (!empty($filter->tagsChecked))
            $query->andWhere(["{{%need_tag}}.tagid" => array_keys($filter->tagsChecked)]);

        // region
        if (!empty($filter->regionid)) {
            $query->andWhere(["{{%church}}.regionid" => $filter->regionid]);

            // subregion
            if (!empty($filter->subregionid)) {
                $query->andWhere(["{{%church}}.subregionid" => $filter->subregionid]);

                // city
                if (!empty($filter->cityid))
                    $query->andWhere(["{{%church}}.cityid" => $filter->cityid]);
            }
        }

        if (!empty($filter->quickOnly))
            $query->andWhere(["{{%need}}.is_quick" => 'Y']);

        // progress filter
        if (!empty($filter->progress))
            $query->having("progress>='".$filter->progress."'");

        //active
        if (!empty($filter->active))
            $query->andWhere(["active" => $filter->active]);

        //Need quick help
        if (!empty($filter->need_help))
            $query->andWhere(["need_help" => $filter->need_help]);

        return $query;
    }
}

