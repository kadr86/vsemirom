<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace common\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;

class Location extends ActiveRecord
{

	public static function tableName()
    {
        return '{{%location}}';
    }

    // validation
    public function rules()
    {
        return [
        ];
    }

    public static function regions()
    {
        return self::find()
            ->where('subregionid=0 AND cityid=0 AND regionid<>0 ')
            ->orderBy("title");
    }
    public static function subregions($regionid)
    {
        return self::find()
            ->where('regionid=:regionid AND subregionid<>0 AND cityid=0',['regionid'=>$regionid])
            ->orderBy("title");
    }
    public static function cities($regionid=null,$subregionid=null)
    {
	
	//List of citites
        if(is_null($regionid) && is_null($subregionid)){
            return self::find()
                ->where('cityid<>0')
                ->orderBy("title");
        }

        return self::find()
            ->where('regionid=:regionid AND subregionid=:subregionid AND cityid<>0',[":regionid"=>$regionid,":subregionid"=>$subregionid])
            ->orderBy("title");
    }

    public static function getRegion($regionid)
    {
        $region = self::findOne(['regionid'=>$regionid]);
        if (!empty($region))
            return $region->name;

        return '';
    }

    public static function getSubregion($regionid,$subregionid)
    {
        $region = self::findOne(['regionid'=>$regionid,'subregionid'=>$subregionid]);
        if (!empty($region))
            return $region->name;

        return '';
    }

    public static function getCity($regionid,$subregionid,$cityid)
    {
        $region = self::findOne(['regionid'=>$regionid,'subregionid'=>$subregionid,'cityid'=>$cityid]);
        if (!empty($region))
            return $region->name;

        return '';
    }

    //
    // helper for class
    //

    /**
     * Getting all location data as list of associated arrays
     *
     * @param int $regionid
     * @param int $subregionid
     * @return list($regions,$subregions,$cities)
     */
    public static function getAllAsArrays($regionid=0,$subregionid=0)
    {
        $regions = yii\helpers\ArrayHelper::map(
            Location::regions()->all(),
            'regionid',
            'title'
        );

        $subregions = $regionid != 0 ?
            yii\helpers\ArrayHelper::map(
                Location::subregions($regionid)->all(),
                'subregionid',
                'title'
            ) : [];

        $cities = $subregionid != 0 ?
            yii\helpers\ArrayHelper::map(
                Location::cities($regionid,$subregionid)->all(),
                'cityid',
                'title'
            ) : [];

        return [$regions,$subregions,$cities];
    }
}
