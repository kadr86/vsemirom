<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace common\models;

use yii\db\ActiveRecord;

class Tag extends ActiveRecord
{
	public $needCount = 0;
    public $checked   = false;

	public static function tableName()
    {
        return '{{%tag}}';
    }

    public function rules()
    {
        return [
            ['name','string','min'=>3,'max'=>'255'],

            ['name','required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название'
        ];
    }

    // TODO: delete need_tag row
}