<?php

/**
 * itech-mobile.ru
 * @author anton
 */

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use common\models\Church;

/**
 * Class Need
 * @package common\models
 */
class Need extends ActiveRecord
{
    protected $tmpTags = 0;

    /**
     * @inheritdoc
     */
	public static function tableName()
    {
        return '{{%need}}';
    }

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['name','string','max'=>100],
            ['description','string','max'=>1000],
            [['name','description'],'required'],
            [['required','collected'],'number'],
            ['tags','safe'],
            ['is_quick','safe']
        ];
    }

    /**
     * Additional fields, that can be expanded
     *
     * @return array
     */
    public function extraFields()
    {
        return ['photos','tags'];
    }

    /**
     * Labels for columns
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'description' => 'Описание',
            'required' => 'Необходимая сумма',
            'collected' => 'Собранная сумма'
        ];
    }

    /**
     * Relation to church
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChurch()
    {
        return $this->hasOne(Church::className(),['id'=>'churchid']);
    }

    /**
     * Relation to tags, assigned to this need
     *
     * @return static
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(),['id'=>'tagid'])->viaTable('{{%need_tag}}',['needid'=>'id']);
    }

    /**
     * Helper for the 'sidebar.twig' template
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(Photo::className(),['churchid'=>'churchid','needid'=>'id']);
    }

    //
    // Utils for assigning tags
    //

    /**
     * Assigning tags temporarily
     *
     * @param $tags
     */
    public function setTags($tags)
    {
        $this->tmpTags = $tags;
    }

    /**
     * Creating tag relations in database
     *
     * @param $tags
     * @throws \yii\db\Exception
     */
    protected function setTmpTags($tags)
    {
        // deleting relations
        Yii::$app->db->createCommand()
            ->delete("{{%need_tag}}","{{%need_tag}}.needid=:needid",[":needid"=>$this->id])
            ->execute();

        // inserting relations
        if (!empty($tags))
        foreach ($tags as $id => $val)
        {
            if (!empty($val))
                Yii::$app->db->createCommand()
                    ->insert("{{%need_tag}}",
                        [
                            "needid" => $this->id,
                            "tagid"  => $id,
                        ])
                    ->execute();
        }
    }

    /**
     * Assigning tag relations after saving
     *
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->setTmpTags($this->tmpTags);
        parent::afterSave($insert,$changedAttributes);

        $this->refresh(); // model will not load new tags without refreshing
    }

    /**
     * Removing tag relations before deleting need
     *
     * @return bool
     * @throws \yii\db\Exception
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete())
        {
            Yii::$app->db->createCommand()
                ->delete("{{%need_tag}}","{{%need_tag}}.needid=:needid",[":needid"=>$this->id])
                ->execute();

            return true;
        } else
            return false;
    }

    /**
     * Searching by filter
     *
     * @param $filter \common\models\SearchFilter
     *
     * @return Query
     */
    public static function search($filter)
    {
        $query = Need::find();
        $query->select([
            "{{%need}}.*",
            "{{%need}}.collected*100/{{%need}}.required as progress"
        ]);

        $query->innerJoin("{{%church}}","{{%need}}.churchid={{%church}}.id");
        if (!empty($filter->tagsChecked))
        {
            $query->innerJoin("{{%need_tag}}","{{%need_tag}}.needid={{%need}}.id");
        }

        // search keyword condition
        if (!empty($filter->keyword))
            $query->andFilterWhere(['or',
                    ['like','{{%need}}.name',$filter->keyword],
                    ['like','{{%church}}.description',$filter->keyword]
                ]
            );

        $query->groupBy("{{%need}}.id");

        if ($filter->churchType != Church::CT_NULL)
            $query->andWhere("type=:type",[":type"=>$filter->churchType]);

        // tags filter
        if (!empty($filter->tagsChecked))
            $query->andWhere(["{{%need_tag}}.tagid" => array_keys($filter->tagsChecked)]);

        // region
        if (!empty($filter->regionid)) {
            $query->andWhere(["{{%church.regionid}}" => $filter->regionid]);

            // subregion
            if (!empty($filter->subregionid)) {
                $query->andWhere(["{{%church.subregionid}}" => $filter->subregionid]);

                // city
                if (!empty($filter->cityid))
                    $query->andWhere(["{{%church.cityid}}" => $filter->cityid]);
            }
        }

        if (!empty($filter->quickOnly))
            $query->andWhere(["{{%need}}.is_quick" => 'Y']);

        // progress filter
        if (!empty($filter->progress))
            $query->having("progress>='".$filter->progress."'");


        return $query;
    }
}