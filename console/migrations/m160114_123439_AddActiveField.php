<?php

use yii\db\Schema;
use yii\db\Migration;

class m160114_123439_AddActiveField extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_church','active', $this->boolean() );
    }

    public function down()
    {
        $this->dropColumn('tbl_church','active');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
