<?php

use yii\db\Schema;
use yii\db\Migration;

class m160114_113752_AddNeedHelpField extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_church','need_help', $this->boolean() );
    }

    public function down()
    {
        $this->dropColumn('tbl_church','need_help' );
    }

}
